#include <iostream>
#include <cstdlib>
#include <string>
using namespace std;

//krüpteerib teksti lükandshiffriga(key on edasilükkamise number)
string cryptText(string text, int key){
    string original, final;
    original = text;
    srand(key);
    for(char &ch : text){
        int random = rand()%50, subtractor = 0;
        if(ch + random > 126) final += "!" + ((int)(ch + random) % 125);
        else {
            final += ch + random;
        }
        
    }
    
    return final;
}


string decryptText(string text, int key){
    string original, final;
    original = text;
    srand(key);
    for(char &ch : text){
        int random = rand()%50, subtractor = 0;
        if(ch - random < 33) final += "~" - (33 - (ch - random));
        else {
            final += ch - random;
        }
    }
}

int main(){
    string word = "!hello", crypted="", decrypted="";
    cout << " original: " << word << endl;
    crypted = cryptText(word, 12);
    cout << "  crypted: " << crypted << endl;
    decrypted = decryptText(crypted, 12);
    cout << "decrypted: " << decrypted << endl;
}
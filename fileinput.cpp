#include <iostream>
#include <fstream>
using namespace std;

int main(){
    ifstream FILE;
    FILE.open("input.txt", ifstream::in);
    //kontrollime kas faili muutjas on midagi sees
    if(!FILE){
        cout << "ERROR: could not open file" << endl;
    }
    //loeme failist andmed sisse
    int buf = 0;
    while(FILE >> buf){
        cout << buf << endl;
    }
    
    FILE.close();
}
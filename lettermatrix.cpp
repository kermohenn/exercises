#include <iostream>
using namespace std;

int main(){
    char array[10][10];
    
    // array fill
    for(int y = 0;y <10;y++){
        for(int x = 0;x < 10;x++){
            //tingimused
            if(y < 2){
                array[x][y] = 'O';
            }
            else if(x >7 && y <8){
                array[x][y] = 'O';
            }
            else if(y > 7 && (x > 0 && x < 9)){
                array[x][y] = 'O';
            }
            else if(x < 2 && y<8 && y>5){
                array[x][y] = 'O';
            }
            else{
                array[x][y] = ' ';
            }
        }
    }
    
    // console output
    for(int y = 0;y <10;y++){
        for(int x = 0;x < 10;x++){
            cout << array[x][y] << " ";
        }
        cout << endl;
    }
}
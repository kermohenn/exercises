#include <stdio.h>
int jookidevalik; //inti abil genereerib täisarvu
float hind; //floati abil genereerib komaga arvu
int jah=8;
int ei=9;
int soov;
float raha;
float lisamine;
float ostukorv;

int main() //tagastab süsteemi väljumise oleku
{
    float kohv=1.00;
    float pepsi=0.90;
    float coka=0.95;
    float vesi=0.50;
    float fanta=0.85;
    float sprite=0.80;
    float limonaad=0.75;



    printf("\nJookide Menuu: "); //prindib ekraanile jookide menuu
    printf("\n\n");
    printf("1. Kohvi   %.2f EUR\n",kohv);
    printf("2. Pepsi   %.2f EUR\n",pepsi);
    printf("3. Coka Cola  %.2f EUR\n",coka);
    printf("4. Vesi    %.2f EUR\n",vesi);
    printf("5. Fanta   %.2f EUR\n",fanta);
    printf("6. Sprite  %.2f EUR\n",sprite);
    printf("7. Limonaad   %.2f EUR\n",limonaad);
    printf("\n\n");

    printf("Sisesta oma valik: ");
    scanf("%d",&jookidevalik );

    switch(jookidevalik) //loob joogivaliku ja annab sellele vajalikud andmed
    {
        case 1:
            printf("Sa valisid kohvi %.2f EUR\n",kohv);
            hind=kohv; //hind võrdub joogiga
            ostukorv+=hind; //iga kord kui juurde ostab lisab hinnale juurde
            break; //ei lähe teistesse cassidesse
        case 2:
            printf("Sa valisid pepsi %.2f EUR\n",pepsi);
            hind=pepsi;
            ostukorv+=hind;
            break;
        case 3:
            printf("Sa valisid coka cola %.2f EUR\n",coka);
            hind=coka;
            ostukorv+=hind;
            break;
        case 4:
            printf("Sa valisid vee %.2f EUR\n",vesi);
            hind=vesi;
            ostukorv+=hind;
            break;
        case 5:
            printf("Sa valisid fanta %.2f EUR\n",fanta);
            hind=fanta;
            ostukorv+=hind;
            break;
        case 6:
            printf("Sa valisid sprite %.2f EUR\n",sprite);
            hind=sprite;
            ostukorv+=hind;
            break;
        case 7:
            printf("Sa valisid limonaadi %.2f EUR\n",limonaad);
            hind=limonaad;
            ostukorv+=hind;
            break;
        default:
            printf("Vale sisestus!\n"); //annab teada, et tehtud vbalik on vale
            return main(); //selle abil kui oli vale valik siis läheb tagasi jookide valiku juurde
            break;
    }

    printf("Kas sa soovid maksta? Praegune summa %.2f EUR! Ei/Jah (8/9): ",ostukorv); //küsib kas soovib maksta ja prindib ka summa mis on ostetud kokku
    scanf("%d",&soov);

    switch(soov)
    {
    case 8: //selle valikuga läheb tagasi valiku juurde
        printf("\nSa soovid veel osta\n");
        return main();
        break;
    case 9: //selle valikuga saab maksta
        printf("\nSa ei soovi enam osta. Summa %.2f EUR",ostukorv);
        printf("\nMakse terminaal: ");
        scanf("%.2f",&raha);
        if (raha<ostukorv) //kui raha on väiksem ostukorvist siis valib selle tegevuse
            {
            do //sellega saab ühe korra tegevust korrata,et ei lähe mitut korda edasi
                {
                    printf("\nMaksa juurde: ");
                    scanf("%f", &lisamine);
                    raha += lisamine; //iga kord lisab juurde rahale summat
                }   while (raha < ostukorv); //see tegevus kestab niikaua kuni tingimus on vale
                if (raha>ostukorv) //kui raha on suurem ostukorvist valib selle tegevuse
                        {
                            printf("\nTagastusraha %.2f EUR", raha-ostukorv);
                            return main();
                        }
                        else if (raha == ostukorv) //kui raha on võrdne ostukorviga valib selle tegevuse
                            {
                                printf("\n Makstud\n");
                                return main();
                            }
            }
            else if (raha>ostukorv)
                    {
                        printf("\n Tagastusraha %.2f EUR", raha - ostukorv);
                        return main();
                    }
                    else if (raha == ostukorv)
                        {
                            printf("\n Head p2eva\n");
                            return main();
                        }
                        break;
    }

    return 0; //siin ta läheb algusesse tagasi kus ta muudab kõik sisestused tagasi nulli
}
#include <iostream> // cout, cin funktsioonid
#include <cstdlib> // srand(), rand() funktsioonid
#include <ctime> // time() funktsioon
using namespace std;

int main(){
    int from = 1, to = 100;
    srand(time(0));
    int random = rand() % to + from;
    // for(int i = 0;i < 10;i++){
    //     random = rand() % 10 +1;
    //     cout << random << endl;
    // }
    while(true){
        int number = 0;
        cout << "Please enter a number(" << from<<"-"<<to<<"): ";
        cin >> number;
        if(number == random){
            cout << "Congratualtions! You guessed it!" << endl;
            return 0;
        }else if(number > random){
            cout << "Too high! Try again." << endl;
        }else if(number < random){
            cout << "Too low! Try again." << endl;
        }
    }
    
}
#include <iostream>
#include <string>
using namespace std;

int main(){
    //if, else if näide
    bool condition = true, condition2 = true;
    if(condition && condition2 || !condition){
        //do something
    }else if(condition2){
        //do something else
    }else{
        //do something if no condition can be met
    }
    
    //switch case näide
    char x = 'a';
    switch(x){
        case 'a':
            cout << "oli a"<< endl;
            break;
        case 'b':
            cout << "oli b"<< endl;
            break;
        default:
            cout << "midagi muud" << endl;
    }
    
    //while näide
    while(true){
        string entry;
        cout << "sisesta: 'q'" << endl;
        cin >> entry;
        //cin.getline(entry,200);
        if(!entry.compare("q")) break;
    }
}